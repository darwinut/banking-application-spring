package com.clientfrontend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientFrontEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientFrontEndApplication.class, args);
	}
}
